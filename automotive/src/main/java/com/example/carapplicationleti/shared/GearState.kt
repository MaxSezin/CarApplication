package com.example.carapplicationleti.shared

enum class GearState(val value:Int,val stringUi:String){
    NEUTRAL(1,"N"),
    REVERSE(2,"R"),
    PARKING(4,"P"),
    DRIVE(8,"D")
}