package com.example.carapplicationleti.shared

import com.example.carapplicationleti.R

enum class IgnitionState(val value: Int,val backgroundColor: Int, val textColor: Int) {
    LOCK(1, R.color.rad30, R.color.rad),
    OFF(2,R.color.gray30, R.color.gray),
    ACC(3,R.color.rad30, R.color.rad),
    ON(4,R.color.green30, R.color.green),
    START(5,R.color.blue30, R.color.blue)
}