package com.example.carapplicationleti.ui

import android.app.Activity
import android.car.Car
import android.car.VehiclePropertyIds.*
import android.car.hardware.CarPropertyValue
import android.car.hardware.property.CarPropertyManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.example.carapplicationleti.R
import com.example.carapplicationleti.databinding.ActivityMainBinding
import com.example.carapplicationleti.shared.GearState.*
import com.example.carapplicationleti.shared.IgnitionState


class MainActivity : Activity() {

    lateinit var binding: ActivityMainBinding


    private lateinit var car: Car
    private lateinit var carPropertyManager: CarPropertyManager

    private var carPropertyListener = object : CarPropertyManager.CarPropertyEventCallback {

        override fun onChangeEvent(value: CarPropertyValue<Any>) {
            when (value.propertyId) {
                PARKING_BRAKE_ON -> {
                    if (value.value is Boolean) {
                        val parkingBrakeIsTernOn = value.value as Boolean
                        if (parkingBrakeIsTernOn) {
                            binding.imgBrakeParking.visibility = View.VISIBLE
                        } else {
                            binding.imgBrakeParking.visibility = View.INVISIBLE
                        }
                    }
                }
                GEAR_SELECTION -> {
                    binding.gearSelection.text = when (value.value) {
                        DRIVE.value -> DRIVE.stringUi
                        REVERSE.value -> REVERSE.stringUi
                        NEUTRAL.value -> NEUTRAL.stringUi
                        PARKING.value -> PARKING.stringUi
                        else -> {
                            ""
                        }
                    }
                }
                IGNITION_STATE -> {
                    binding.apply {
                        when (value.value) {
                            IgnitionState.ACC.value -> {
                            }
                            IgnitionState.OFF.value -> {
                                setSelectedStyle(cardViewStateOff, textOff, IgnitionState.OFF)
                                setDefaultStyle(
                                    listOf(
                                        Pair(cardViewStateStart, textStart),
                                        Pair(cardViewStateLock, textLock),
                                        Pair(cardViewStateOn, textOn),
                                    )
                                )
                            }
                            IgnitionState.LOCK.value -> {
                                setSelectedStyle(cardViewStateLock, textLock, IgnitionState.LOCK)
                                setDefaultStyle(
                                    listOf(
                                        Pair(cardViewStateStart, textStart),
                                        Pair(cardViewStateOff, textOff),
                                        Pair(cardViewStateOn, textOn),
                                    )
                                )
                            }
                            IgnitionState.ON.value -> {
                                setSelectedStyle(cardViewStateOn, textOn, IgnitionState.ON)
                                setDefaultStyle(
                                    listOf(
                                        Pair(cardViewStateStart, textStart),
                                        Pair(cardViewStateLock, textLock),
                                        Pair(cardViewStateOff, textOff),
                                    )
                                )
                            }
                            IgnitionState.START.value -> {
                                setSelectedStyle(cardViewStateStart, textStart, IgnitionState.START)
                                setDefaultStyle(
                                    listOf(
                                        Pair(cardViewStateOff, textOff),
                                        Pair(cardViewStateLock, textLock),
                                        Pair(cardViewStateOn, textOn),
                                    )
                                )
                            }
                        }
                    }
                }

                ENV_OUTSIDE_TEMPERATURE -> {

                        if (value.value as Float >= 40.0) Toast.makeText(
                            applicationContext,
                            "Temperature so height!",
                            Toast.LENGTH_SHORT
                        ).show()

                    binding.textTemperature.text = value.value.toString()
                }
            }

        }

        private fun setDefaultStyle(listOf: List<Pair<CardView, TextView>>) {
            listOf.forEach {
                it.first.setCardBackgroundColor(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.default_background50
                    )
                )
                it.second.setTextColor(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.default_gray
                    )
                )
            }
        }

        override fun onErrorEvent(propId: Int, zone: Int) {
            Toast.makeText(
                this@MainActivity.applicationContext,
                "Was Error with propId: $propId",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun setSelectedStyle(
        cardView: CardView,
        textOff: TextView,
        ignitionState: IgnitionState
    ) {
        textOff.setTextColor(ContextCompat.getColor(applicationContext, ignitionState.textColor))
        cardView.setCardBackgroundColor(
            ContextCompat.getColor(
                applicationContext,
                ignitionState.backgroundColor
            )
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)
        initCarPropertyManager()
    }

    private fun initCarPropertyManager() {
        car = Car.createCar(this)
        carPropertyManager = car.getCarManager(Car.PROPERTY_SERVICE) as CarPropertyManager;

        listCarRequest.forEach { parameter ->
            carPropertyManager.registerCallback(
                carPropertyListener,
                parameter,
                CarPropertyManager.SENSOR_RATE_ONCHANGE
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        car.disconnect()
    }

    companion object {
        val listCarRequest = listOf(
            PARKING_BRAKE_ON,
            GEAR_SELECTION,
            CURRENT_GEAR,
            IGNITION_STATE,
            PERF_VEHICLE_SPEED,
            FUEL_LEVEL,
            HVAC_TEMPERATURE_CURRENT,
            ENV_OUTSIDE_TEMPERATURE
        )
    }
}
